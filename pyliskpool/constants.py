#!/usr/bin/env python3

import sys
import yaml

class constants():
    """
    Constant variables
    """
    try:
        with open('./config.yaml') as fh:
            data = yaml.safe_load(fh)
        with open('./exception_list') as el:
            ignore = yaml.safe_load(el)
    except IOError:
        raise
        sys.exit("Cannot open configuration file in constants.py")

    url = data.get('url')

    headers = {"Content-Type": "application/json"}

    getforgedby_account = '/api/delegates/forging/getForgedByAccount?generatorPublicKey='
    getaccount_voters = '/api/voters?address='
    transactions_uri = '/api/transactions'
    delegate_address = '17589464102226817833L'

    min_payout = data.get('min_payout')

    db_file = './pyliskpool.sqlite'
