In the root dir of this repo run the pool in dry-run mode

```
python3 pool.py --dry-run --percentage 35
```

Copy the results of the dry run to the offline directory
```
cp 2018-02-20-details.log offline
cd offline
```

Make the offline transactions
```
python3 make_offline.py
```

Send the transactions
```
python3 pyliskcrypt.py send --url https://node01.lisk.io --file-path transaction24.json --nethash deadbeef123 --version 0.9.8
python3 pyliskcrypt.py send --url https://node01.lisk.io --file-path transaction49.json --nethash deadbeef123 --version 0.9.8
...
```
