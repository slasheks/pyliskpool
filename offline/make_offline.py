#!/usr/bin/env python3

import os
import re
import sys
import json
import argparse
import getpass
import pyliskcrypt
from ast import literal_eval

def parse_details_log(secret1, secret2, filename, timestamp, fee):
    """
    """
    with open(filename) as logfh:

        transactions = []

        # Skip the first 6 lines they are just info
        data = logfh.readlines()[6:]

        pubk, privk, pubk2, privk2 = pyliskcrypt.get_pub_priv_key(secret1, secret2)

        for line, match in enumerate(data):

            line_match = re.search(r'(\S+) (\S+) (\S+) (.*)',
                                   match,
                                   re.MULTILINE | re.IGNORECASE)
            if line_match:
                try:
                    data_dict = literal_eval(line_match.group(4))
                except SyntaxError:
                    sys.exit('Found a bad line. Make sure the file is correct')

                resp = pyliskcrypt.gen_tx(pubk,
                                          privk,
                                          pubk2,
                                          privk2,
                                          data_dict['payout_amount'],
                                          data_dict['wallet_address'],
                                          timestamp,
                                          fee)

                transactions.append(resp)

    return transactions

def main(args):

    secret1a = None
    secret2a = None

    try:
        print("Please the first passphrase:")
        secret1a = getpass.getpass()
        print("Confirming the passphrase. Please type/paste it again.")
        secret1b = getpass.getpass()

        # Simple check. needs revamp
        if secret1a != secret1b:
            sys.exit("Passprase does not match. Please try again")
        else:
            del secret1b

        if args.second_secret:
            print("Please the second passphrase:")
            secret2a = getpass.getpass()
            print("Confirming the second passphrase. Please type/paste it again.")
            secret2b = getpass.getpass()

            # Simple check. needs revamp
            if secret2a != secret2b:
                sys.exit("Passprase does not match. Please try again")
            else:
                del secret2b

    except KeyboardInterrupt:
        sys.exit("Good Bye Steven")

    for root, dirs, files in os.walk(".", topdown=False):

        for name in files:

            if 'details' in name:
                print("Parsing {}".format(name))
                transactions = parse_details_log(secret1a,
                                                 secret2a,
                                                 os.path.join(root, name),
                                                 args.genesis_timestamp,
                                                 args.fee)
    # Save the transaction object to file
    step = 24
    tx_new = []
    if len(transactions) <= 25:
        with open('transaction{}.json'.format(step), 'w') as jfh:
            json.dump({"transactions":transactions}, jfh)
            sys.exit()


    for idx, transaction in enumerate(transactions):
        tx_new.append(transaction)
        print(idx)
        if idx == step:
            with open('transaction{}.json'.format(step), 'w') as jfh:
                json.dump({"transactions":tx_new}, jfh)
            step += 25
            tx_new = []

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser()

    PARSER.add_argument('-s','--second-secret',
                        dest='second_secret',
                        action='store_true',
                        help='')

    PARSER.add_argument('-t',
                        '--timestamp',
                        dest='genesis_timestamp',
                        action='store',
                        type=int,
                        default=1464109200,
                        help='')

    PARSER.add_argument('--fee',
                        dest='fee',
                        action='store',
                        type=int,
                        default=10000000,
                        help='')


    ARGS = PARSER.parse_args()

    main(ARGS)
