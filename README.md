

./pool.py -h

usage: 

    Provide the public key of the delegate and when to start
    Also need the passphrase, you will be prompted

    pool.py -p <publickey> -t <start timestamp unix>

    Do not perform an action, just print what would have been payed out
    pool.py -p <publickey> -t <start timestamp unix> --dry-run
    

LISK Pool Sample code

optional arguments:

  -h, --help            show this help message and exit

  --second-pass         Second passphrase flag

  -d, --dry-run         Dry run flag

  -p PUBKEY, --public-key PUBKEY
                        Delegate Public key

  --percentage PERCENTAGE
                        Reward percentage

  -t TIMESTAMP, --timestamp TIMESTAMP
                        Timestamp to start

  -i INTERVAL, --interval INTERVAL
                        How often to send transactions in seconds

  -f FILENAME, --filename FILENAME
                        Config file. Default is ./config.yaml
