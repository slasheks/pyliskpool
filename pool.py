#!/usr/bin/env python3

# TODO sort by weight so that whales are paid first
# TODO what if the rewards are less than the transaction fees?
# TODO add database with values to write (every hour then take the average of vote)
# TODO need a way to determine the start of the new payout schedule
# TODO need to know which is the last payout to grab the voters
# TODO add an init flag that will create the database with required tables for first use

import sys
import math
import time
import argparse
import getpass
import logging
import yaml
import pyliskpool

def main(second_phrase, public_key, timestamp, filename, dry_run, interval,
         percentage):
    """
    Submain function for scoping
    """
    # set the pubkey in the class to be read by other methods
    pyliskpool.constants.pubkey = public_key

    cursor, conn = pyliskpool.utils.open_db()

    pay_wave = pyliskpool.accounting.voter_rewards(timestamp, percentage,
                                                   dry_run, cursor, conn)

    secret1_1 = None
    secret2_1 = None

    if not dry_run:

        # prompt for the secret for the payout
        try:
            secret1_1 = getpass.getpass()
            print("Verifying first passphrase. Please enter it again:")
            secret1_2 = getpass.getpass()

            if secret1_1 != secret1_2:
                sys.exit("First pass does not match. please try again.")
        except KeyboardInterrupt:
            sys.exit("Operation aborted. No phrase given")

        if second_phrase:
            try:
                secret2_1 = getpass.getpass()
                print("Verifying second passphrase. Please enter it again:")
                secret2_2 = getpass.getpass()
            except KeyboardInterrupt:
                sys.exit("Operation aborted. No 2nd phrase given")

            if secret2_1 != secret2_2:
                sys.exit("Second pass does not match. please try again.")

    transactions, payout_time = pyliskpool.transfer.payouts(cursor,
                                                            conn,
                                                            pay_wave,
                                                            interval,
                                                            secret1_1,
                                                            secret2_1,
                                                            dry_run)

    # Get the data from the config file and add the last payout time
    data = _get_data_file(filename)
    data['last_payout_time'] = payout_time.get('last_payout_time')

    # Write back to the yaml file if not dry run
    if not dry_run:

        _write_data_file(filename, data)

    pyliskpool.utils.close_db(conn)



def _get_data_file(filename):
    """

    """
    try:
        with open(filename) as fh:
            data = yaml.safe_load(fh)
            return data
    except IOError:

        sys.exit("")

def _write_data_file(filename, data):
    """

    """
    try:

        with open(filename, 'w') as fh:

            yaml.safe_dump(data, fh, allow_unicode=True,
                           default_flow_style=False)

            return True

    except IOError:

        sys.exit("")


def usage():

    return '''

    Provide the public key of the delegate and when to start
    Also need the passphrase, you will be prompted

    pool.py -p <publickey> -t <start timestamp unix>

    Do not perform an action, just print what would have been payed out
    pool.py -p <publickey> -t <start timestamp unix> --dry-run
    '''

if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='LISK Pool Sample code',
                                     usage=usage())

    PARSER.add_argument('--second-pass', dest='second_phrase',
                        action='store_true', default=False,
                        help='Second passphrase flag')

    PARSER.add_argument('-d', '--dry-run', dest='dry_run',
                        action='store_true', default=False,
                        help='Dry run flag')

    PARSER.add_argument('-p', '--public-key', dest='pubkey',
                        action='store', help='Delegate Public key')

    PARSER.add_argument('--percentage', dest='percentage', type=int,
                        default=10, action='store', help='Reward percentage')

    PARSER.add_argument('-t', '--timestamp', dest='timestamp',
                        action='store', help='Timestamp to start')

    PARSER.add_argument('-i', '--interval', dest='interval', type=float,
                        default=0.5, action='store',
                        help='How often to send transactions in seconds')

    PARSER.add_argument('-f', '--filename', dest='filename',
                        default="./config.yaml", action='store',
                        help='Config file. Default is ./config.yaml')

    ARGS = PARSER.parse_args()

    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    yaml_data = _get_data_file(ARGS.filename)

    if not ARGS.timestamp:
        if yaml_data.get('last_payout_time'):
            ARGS.timestamp = yaml_data.get('last_payout_time')
        else:
            PARSER.print_help()
            sys.exit("No start timestamp defined, cannot continue")
    if not ARGS.pubkey:
        if yaml_data.get('pubkey'):
            ARGS.pubkey = yaml_data.get('pubkey')
        else:
            PARSER.print_help()
            sys.exit("No public key defined, cannot continue")

    main(ARGS.second_phrase, ARGS.pubkey, ARGS.timestamp, ARGS.filename,
         ARGS.dry_run, ARGS.interval, ARGS.percentage)
